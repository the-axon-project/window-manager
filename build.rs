// The Window Manager for The Axon Project
// Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![forbid(clippy::all)]
#![forbid(clippy::nursery)]
#![forbid(clippy::pedantic)]
#![forbid(clippy::cargo)]
#![forbid(nonstandard_style)]
#![forbid(warnings)]
#![forbid(rust_2018_idioms)]
#![forbid(unused)]
#![forbid(future_incompatible)]
#![forbid(box_pointers)]
#![forbid(elided_lifetimes_in_paths)]
#![forbid(missing_copy_implementations)]
#![forbid(missing_debug_implementations)]
#![forbid(missing_docs)]
#![forbid(single_use_lifetimes)]
#![forbid(trivial_casts)]
#![forbid(trivial_numeric_casts)]
#![forbid(unused_import_braces)]
#![forbid(unused_qualifications)]
#![forbid(unused_results)]
#![forbid(variant_size_differences)]

//! Build script

/// Entry point for the program
pub fn main() {}
