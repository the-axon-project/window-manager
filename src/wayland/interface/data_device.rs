// Wayland Buffer Interfaces
// Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_data_device;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_data_device::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_data_device::requests = wl_data_device::requests {
    start_drag: Some(start_drag),
    set_selection: Some(set_selection),
    release: Some(release),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_device: destroy_resource"
    );
    core::todo!();
}

extern "C" fn start_drag(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    source: Option<NonNull<resource>>,
    origin: NonNull<resource>,
    icon: Option<NonNull<resource>>,
    serial: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_device: start_drag"
    );
    core::todo!();
}

extern "C" fn set_selection(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    source: Option<NonNull<resource>>,
    serial: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_device: set_selection"
    );
    core::todo!();
}

extern "C" fn release(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_device: release");
    core::todo!();
}
